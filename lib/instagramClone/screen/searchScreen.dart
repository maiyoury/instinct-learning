import 'package:flutter/material.dart';
import 'package:flutter_app2222/instagramClone/widget/searchCategorie.dart';
import 'package:flutter_app2222/instagramClone/itemInstagram.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  double _widthOfScreen;
  double _heightOfScreen;
  @override
  Widget build(BuildContext context) {
    _widthOfScreen = MediaQuery.of(context).size.width;
    _heightOfScreen = MediaQuery.of(context).size.height;
    return Scaffold(
      body: _buildBody,
    );
  }
  get _buildBody2{
    return Container(
      child: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          SearchCategorie(),
        ],
      ),
    );
  }

  get _buildBody{
    return CustomScrollView(
      slivers: [
        _buildSliverSearchBar,
        _buildSliverAppBar,
        _buildSliverGrid,
        _buildSliverGrid,
      ],
    );
  }

  get _buildSliverSearchBar{
    return SliverAppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      elevation: 0.0,
      automaticallyImplyLeading: false,
      pinned: false,
      floating: false,
      title: TextField(
        style: Theme.of(context).primaryTextTheme.caption,
        decoration: InputDecoration(
          /*border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0))),*/
          prefixIcon: Icon(Icons.search),
          hintText: 'Search',
        ),
      ),
    );
  }
  get _buildSliverAppBar{
    return SliverAppBar(
      title: SearchCategorie(),
      centerTitle: true,
      pinned: true,
      expandedHeight: 80,
      backgroundColor: Colors.white,
    );
  }
  get _buildSliverList2{
    return  SliverList(
      delegate: SliverChildListDelegate(List.generate(
          100,
              (i) => ListTile(
            title: Text('Scroll '),
          )).toList()),
    );
  }
  get _buildSliverList{
    return SliverAppBar(
        expandedHeight: 150.0,
        flexibleSpace: const FlexibleSpaceBar(
          title: Text('Available seats'),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_circle),
            tooltip: 'Add new entry',
            onPressed: () {

            },
          ),
        ]
    );
  }
  get _buildSliverGrid{
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200.0,
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 10.0,
        childAspectRatio: 4.0,
      ),
      delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
          return Container(
            alignment: Alignment.center,
            color: Colors.teal[100 * (index % 9)],
            child: Text('Grid Item $index'),
          );
          //return _buildListSearchItem(itemStoryList[index]);
        },
        childCount: 20,
      ),
    );
  }
  _buildListSearchItem(ItemInstagram url){
    return Container(
      width: _widthOfScreen,
      height: _heightOfScreen,
      child: Image.network(url.img),
    );
  }
}
