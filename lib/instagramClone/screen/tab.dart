import 'package:flutter/material.dart';
import 'package:flutter_app2222/instagramClone/model/iconTabButtonModel.dart';
import 'package:flutter_app2222/instagramClone/screen/instagramHomeScreen.dart';
import 'package:flutter_app2222/instagramClone/screen/profileScreen.dart';
import 'package:flutter_app2222/instagramClone/screen/activityScreen.dart';
import 'package:flutter_app2222/instagramClone/screen/searchScreen.dart';
import 'package:flutter_app2222/instagramClone/screen/addPostScreen.dart';


class TabCustom extends StatefulWidget {
  @override
  _TabCustomState createState() => _TabCustomState();
}

class _TabCustomState extends State<TabCustom> {

  int currentTap = 0;
  //tab to screen
  Widget buildTapBody(int current){
    switch (current) {
      case 0:
        return new InstagramHomeScreen();
        break;
      case 1:
        return new SearchScreen();
        break;
      case 2:
        return new AddPostScreen();
        break;
      case 3:
        return new ActivityScreen();
        break;
      default:
        return ProfileScreen();
        break;
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildTapBody(currentTap),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.black87,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        unselectedItemColor: Colors.black87,
        currentIndex: currentTap,
        onTap: (val){
          setState(() {
            currentTap = val;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: ''
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search_outlined),
            label: ''
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_box_outlined),
            label: ''
          ),
          BottomNavigationBarItem(
            icon: Icon(currentTap == 3 ? Icons.favorite : Icons.favorite_border),
            label: ''
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_pin),
            label: ''
          ),
        ],
      )
    );
  }
}
