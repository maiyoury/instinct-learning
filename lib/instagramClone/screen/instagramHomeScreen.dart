import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_app2222/instagramClone/itemInstagram.dart';
import 'package:flutter_app2222/instagramClone/y_global.dart' as yStyle;
ItemInstagram itemInstagram;



class InstagramHomeScreen extends StatefulWidget {
  @override
  _InstagramHomeScreenState createState() => _InstagramHomeScreenState();
}

class _InstagramHomeScreenState extends State<InstagramHomeScreen> {
  double _widthOfScreen;
  double _heightOfScreen;


  @override
  Widget build(BuildContext context) {
    _widthOfScreen = MediaQuery.of(context).size.width;
    _heightOfScreen = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: _buildAppBar,
      body: _buildBody2,
    );
  }


  get _buildAppBar{
    return AppBar(
      backgroundColor: Colors.grey[200],
      title: Image.network('https://lh3.googleusercontent.com/proxy/-nrUdccSkqckzEfjuC2eWB8rpiFy145WxTPIRruRsZJXo4NDX_VvGQtzB5S8Kb1hQ-LFCY4m6O-w6EJhe-ffRxCONUOAnxKf7MtZ6RwWvQkKrkJZX49K2K4J2Oop'),
      centerTitle: true,
      leading: IconButton(icon: Icon(Icons.camera_alt_outlined, color: Colors.black54,), onPressed: (){
        print('Button Camera');
      }),
      actions: [
        IconButton(icon: Icon(FontAwesomeIcons.facebookMessenger, color: Colors.black54,), onPressed: (){
          print('Button Message');
        }),
      ],
    );
  }
  get _buildBody2{
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          //mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: 80,
              child: _buildStory,
            ),
            Column(
              children: List.generate(12, (index) {
                return _buildPostContent;
              })
            ),
          ],
        ),
      ),
    );
  }

  get _buildPostContent{
    return Container(
      child: Column(
        children: [
          _buildPostName,
          _buildPreviewRow,
          _buildIconLoveComment,
          buildTextComment(itemStoryList[0].img),
        ],
      ),
    );
  }
   buildTextComment(String url){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FlatButton(
            child: Text("12 likes", style: yStyle.textStyleBold),
            onPressed: () {

            },
          ),
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 15, right: 10),
                child: Text('Mai Youry',
                  // post.user.username,
                  style: yStyle.textStyleBold,
                ),
              ),
              Text('Love',
                //post.description,
                style: yStyle.textStyle,
              )
            ],
          ),
          FlatButton(
            child: Text("View all 3 comments", style: yStyle.textStyleLigthGrey,),
            onPressed: () {

            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20,),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Stack(
                  alignment: Alignment(0,0),
                  children: [
                    Container(
                      height: 30,
                      width: 30,
                      child: CircleAvatar(
                        //backgroundColor: Colors.red,
                      ),
                    ),
                    Container(
                      height: 28,
                      width: 28,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                      ),
                    ),
                    Container(
                        height: 30,
                        width: 30,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: Image.network(url,),
                        )
                    ),
                  ],
                ),
                Container(
                  child: Text('Add a comment', style: yStyle.textStyleLigthGrey,),
                ),
                Container(
                  child: Row(
                    children: [
                      IconButton(icon: Icon(FontAwesomeIcons.heart, color: Colors.red, size: 17,), onPressed: null),
                      IconButton(icon: Icon(FontAwesomeIcons.heart, color: Colors.red, size: 17,), onPressed: null),
                      IconButton(icon: Icon(FontAwesomeIcons.heart, color: Colors.red, size: 17,), onPressed: null),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  get _buildIconLoveComment{
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: [
            Row(
              children: <Widget>[
                Stack(
                  alignment: Alignment(0, 0),
                  children: <Widget>[
                    //Icon(FontAwesomeIcons.heart, size: 30, color: Colors.black,),
                    IconButton(icon: Icon(FontAwesomeIcons.heart), color: Colors.black87,
                      onPressed: () {

                      },)
                  ],
                ),
                Stack(
                  alignment: Alignment(0, 0),
                  children: <Widget>[
                    //Icon(FontAwesomeIcons.comment, size: 30, color: Colors.black,),
                    IconButton(icon: Icon(FontAwesomeIcons.comment), color: Colors.black87,
                      onPressed: () {

                      },)
                  ],
                ),
                Stack(
                  alignment: Alignment(0, 0),
                  children: <Widget>[
                    //Icon(FontAwesomeIcons.paperPlane, size: 30, color: Colors.black,),
                    IconButton(icon: Icon(FontAwesomeIcons.paperPlane), color: Colors.black87,
                      onPressed: () {

                      },)
                  ],
                ),
              ],
            ),
            SizedBox(width: MediaQuery.of(context).size.width / 2.2,),
            Stack(
              alignment: Alignment(0,0),
              children: <Widget>[
                //Icon(Icons.bookmark, size: 30, color: Colors.black,),
                IconButton(icon: Icon(Icons.bookmark_border), color: Colors.black87,
                  onPressed: () {

                  },)
              ],
            ),
          ],
        ),
      ],
    );
  }
  get _buildStory{
    return Expanded(
      child: Container(
        //color: Colors.blueGrey,
        height: 70,
        width: _widthOfScreen,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: itemStoryList.length,
            itemBuilder: (context, index){
              return _buildListItem(itemStoryList[index]);
            }
        ),
      ),
    );
  }

  get _buildPreviewRow{
    return Container(
      //color: Colors.grey,
      height: 250,
      width: _widthOfScreen,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: itemStoryList.length,
          itemBuilder: (context, index){
            return _buildPreviewListItem(itemStoryList[index]);
          }
      ),
    );
  }

  get _buildPostName {
    return Container(
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.start,
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Stack(
                  alignment: Alignment(0,0),
                  children: [
                    Container(
                      height: 35,
                      width: 35,
                      child: CircleAvatar(
                        backgroundColor: Colors.red,
                      ),
                    ),
                    Container(
                      height: 33,
                      width: 33,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                      ),
                    ),
                    Container(
                        height: 35,
                        width: 35,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: Image.network('https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-9/133565142_2800964720222988_819032698398492943_o.jpg?_nc_cat=106&ccb=2&_nc_sid=110474&_nc_ohc=WhDxmrn32_8AX9w_uKN&_nc_ht=scontent.fpnh3-1.fna&oh=e9a93fff47a0d536b16662704775a938&oe=6022FB97'),
                        )
                    ),
                  ],
                ),
              ),

              SizedBox(width: 15,),
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Mesa', style: TextStyle(fontWeight: FontWeight.bold),),
                      SizedBox(height: 2,),
                      Text('Bangkok, Thailand', textAlign: TextAlign.start, style: TextStyle(color: Colors.grey[900], fontSize: 12),),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: (){
                  final action = CupertinoActionSheet(
                    actions: <Widget>[
                      CupertinoActionSheetAction(
                        child: Text("Report"),
                        isDestructiveAction: true,
                        onPressed: () {
                          print("Action 1 is been clicked");
                        },
                      ),
                      CupertinoActionSheetAction(
                        child: Text("Copy Link"),
                        onPressed: () {
                          print("Action 2 is been clicked");
                        },
                      ),
                      CupertinoActionSheetAction(
                        child: Text("Share to..."),
                        onPressed: () {
                          print("Action 2 is been clicked");
                        },
                      ),
                      CupertinoActionSheetAction(
                        child: Text("Torn On Post Notification"),
                        onPressed: () {
                          print("Action 2 is been clicked");
                        },
                      ),
                      CupertinoActionSheetAction(
                        child: Text("Mute"),
                        onPressed: () {
                          print("Action 2 is been clicked");
                        },
                      ),
                      CupertinoActionSheetAction(
                        child: Text("Unfollow"),
                        onPressed: () {
                          print("Action 2 is been clicked");
                        },
                      ),
                    ],
                    cancelButton: CupertinoActionSheetAction(
                      child: Text("Cancel"),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  );
                  showCupertinoModalPopup(
                      context: context, builder: (context) => action);
                },
                child: Container(
                  padding: EdgeInsets.only(right: 5, top: 5),
                  child: Icon(Icons.more_horiz),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _buildPreviewListItem(ItemInstagram item){
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          height: _heightOfScreen,
          width: _widthOfScreen,
          margin: EdgeInsets.all(5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(1),
            image: DecorationImage(
              image: NetworkImage(item.img),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ],
    );
  }

  _buildListItem(ItemInstagram item){
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          height: 50,
          width: 50,
          child: CircleAvatar(
            backgroundColor: Colors.red,
          ),
        ),
        Container(
          height: 47,
          width: 47,
          child: CircleAvatar(
            backgroundColor: Colors.white,
          ),
        ),
        Container(
          width: 47,
          height: 47,
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            image: DecorationImage(
                image: NetworkImage(item.img),fit: BoxFit.cover),
          ),
        ),
        Positioned(
          bottom: -10,
          child: Container(
            height: 30,
            width: 150,
            alignment: Alignment.center,
            child: Text(item.title, style: TextStyle(fontSize: 10,),),
          ),
        ),
        SizedBox(height: 20,),
      ],
    );
  }
}
