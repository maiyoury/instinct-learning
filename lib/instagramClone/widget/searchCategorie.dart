import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
class SearchCategorie extends StatelessWidget {
  String path = 'assets/img/cats/';
  List menuCategories = [
    {
      'id' : 1,
      'name' : 'IGTV ',
    },
    {
      'id' : 2,
      'name' : 'Shop',
    },
    {
      'id' : 3,
      'name' : 'Travel',
    },
    {
      'id' : 4,
      'name' : 'Architecture',
    },
    {
      'id' : 5,
      'name' : 'Decor',
    },
    {
      'id' : 6,
      'name' : 'Style',
    },
    {
      'id' : 7,
      'name' : 'Food',
    },
    {
      'id' : 8,
      'name' : 'Art',
    },
    {
      'id' : 9,
      'name' : 'DIY',
    },
    {
      'id' : 10,
      'name' : 'Beauty',
    },
    {
      'id' : 11,
      'name' : 'TV & Movies',
    },
    {
      'id' : 12,
      'name' : 'Sports',
    },
    {
      'id' : 13,
      'name' : 'Comics',
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.blueGrey,
      height: 30,
      alignment: Alignment.center,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: List.generate(menuCategories.length, (index){
          return Container(
            margin: EdgeInsets.only(right: 8, left: 10),
            decoration: BoxDecoration(
              //color: Colors.grey,
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: Colors.black87)
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child: Text('${menuCategories[index]['name']}', style: TextStyle(fontSize: 12, color: Colors.black87),),
            ),
          );
        }),
      ),
    );
  }
}
