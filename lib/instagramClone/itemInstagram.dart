class ItemInstagram {
  String title, img, content;

  ItemInstagram({this.title, this.img, this.content});
}

List<ItemInstagram> itemList = [
  ItemInstagram(
    title: "Tong Hana",
    content: "The saga of the Eternals, a race of immortal beings who lived on Earth and shaped its history and civilisations.",
    img:"https://i.pinimg.com/originals/9b/97/87/9b9787fb5209b99c3207554a341e3f32.png",
  ),
  ItemInstagram(
    title: "Ton Bopha",
    content: "The saga of the Eternals, a race of immortal beings who lived on Earth and shaped its history and civilisations.",
    img:"https://i.pinimg.com/originals/9b/97/87/9b9787fb5209b99c3207554a341e3f32.png",
  ),
  ItemInstagram(
    title: "Ti Dara",
    content: "The saga of the Eternals, a race of immortal beings who lived on Earth and shaped its history and civilisations.",
    img:"https://4.bp.blogspot.com/_QrJgy6QEth8/SxJ4X4HIbNI/AAAAAAAAGkk/Uq7L0T870wo/s1600/khmer_singer_Sok_sreyneang_red_dress_01.jpg",
  ),
];

List<ItemInstagram> itemStoryList = [
  ItemInstagram(
    title: "Tong Hana",
    content: "Pete \"Maverick\" Mitchell keeps pushing the envelope after years of service as one of the Navy's top aviators. He must soon confront the past while training a new squad of graduates for a dangerous mission that demands the ultimate sacrifice.",
    img: "https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-9/135201604_2805585953094198_7178848251944334749_o.jpg?_nc_cat=109&ccb=2&_nc_sid=110474&_nc_ohc=z4wGQ13PEsMAX-VBAYo&_nc_ht=scontent.fpnh3-1.fna&oh=8a119f040c9d1f53b74bb50d635d6cb5&oe=60215312",
  ),
  ItemInstagram(
    title: "Ton Bopha",
    content: "នំបុព្វបុរសរបស់យើង... សូមចូលរួមលើកតម្កើងទាំងអស់គ្មា #អាពាហ៌ពិពាហ៍ #នំខ្មែរ — at នាឡិ - Neal.",
    img:"https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-9/133292177_2802046210114839_1025518568099204657_o.jpg?_nc_cat=110&ccb=2&_nc_sid=110474&_nc_ohc=mbXZmVitojsAX-YMw99&_nc_ht=scontent.fpnh3-1.fna&oh=ecbd2384e57340e14fee64c4f588418f&oe=60247FA2",
  ),
  ItemInstagram(
    title: "Ti Dara",
    content: "កន្រ្តក់អនុសានុស្សាវរិ៍យដ៏មានគុណតម្លៃសម្រាប់វប្បធម៌ សេដ្ឋកិច្ច និងភាពថ្លៃថ្នូ...។ សូមជ្រើសរើសនំខ្មែរជាការដូសម្រាប់ឆ្នាំថ្មីរបស់លោក លោកស្រី។",
    img:"https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-9/133565142_2800964720222988_819032698398492943_o.jpg?_nc_cat=106&ccb=2&_nc_sid=110474&_nc_ohc=WhDxmrn32_8AX9w_uKN&_nc_ht=scontent.fpnh3-1.fna&oh=e9a93fff47a0d536b16662704775a938&oe=6022FB97",
  ),
  ItemInstagram(
    title: "Ti Dara",
    content: "គ្រោងរៀបអាពាហ៌ពិពាហ៌ឆ្នាំថ្មីមែនទេ នេះនែ៎នំខ្មែរដែលជាទីទុក្ខចិត្តរបស់លោកអ្នក... សូមជូនពរមានសុភមង្គល់ #អាពាហ៌ពិពាហ៍ #នំខ្មែរប្រពៃណី #នំបុរាណ",
    img:"https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-9/133125990_2799355363717257_917311130060216601_o.jpg?_nc_cat=100&ccb=2&_nc_sid=110474&_nc_ohc=sKG3MfEZ9a8AX_DCUAc&_nc_ht=scontent.fpnh3-1.fna&oh=f2197d31b5e53bb56a6d2246f72f3990&oe=6023BBB5",
  ),
  ItemInstagram(
    title: "Ti Dara",
    content: "អត្ថប្រយោន៍ អន្សមមៀន សម្រាប់សុខភាព ១. អង្ករដំណើប​: ប្រសិនជា​អ្នក​ចង់​ឲ្យ​រាងកាយ​អ្នក​អាច​បញ្ចេញ​ជាតិពុល​ចេញមកក្រៅ អ្នក​អាច​ញ៉ាំ​អង្ករដំណើប​នេះ​បាន ព្រោះ​វា​មាន​ប្រូតេអ៊ីន កាលស្យូម វីតាមីន​B2 និង​សារធាតុចិញ្ចឹម​ជាច្រើន​ទៀត​។​",
    img:"https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-9/s960x960/131076821_2792748377711289_7770281108541286277_o.jpg?_nc_cat=101&ccb=2&_nc_sid=110474&_nc_ohc=_Ow1Fc1kZ6IAX_-Vm9a&_nc_ht=scontent.fpnh3-1.fna&tp=7&oh=a565221e84d8600d684b26d97a7f7529&oe=60216B00",
  ),
  ItemInstagram(
    title: "Ti Dara",
    content: "The saga of the Eternals, a race of immortal beings who lived on Earth and shaped its history and civilisations.",
    img:"https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-9/s960x960/130186928_2783480688638058_1672554372505120856_o.jpg?_nc_cat=100&ccb=2&_nc_sid=110474&_nc_ohc=O9zT8Py6hX8AX98xzCd&_nc_ht=scontent.fpnh3-1.fna&tp=7&oh=6bebba4c8791888a5100b0c4cd339fd6&oe=602237BE",
  ),
  ItemInstagram(
    title: "Ti Dara",
    content: "The saga of the Eternals, a race of immortal beings who lived on Earth and shaped its history and civilisations.",
    img:"https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-0/p640x640/127775941_908426483290056_4961872667595750016_o.jpg?_nc_cat=100&ccb=2&_nc_sid=110474&_nc_ohc=iO7akOaJYJgAX8MyCJj&_nc_ht=scontent.fpnh3-1.fna&tp=6&oh=e048fa5caaab0cd9f560bd7ba10f3cca&oe=6021A733",
  ),
  ItemInstagram(
    title: "Ti Dara",
    content: "The saga of the Eternals, a race of immortal beings who lived on Earth and shaped its history and civilisations.",
    img:"https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-0/p640x640/127231963_908426506623387_1383066282835817165_o.jpg?_nc_cat=107&ccb=2&_nc_sid=110474&_nc_ohc=YmV0BcG6BAMAX9DSvf2&_nc_ht=scontent.fpnh3-1.fna&tp=6&oh=e6f4bca8081f5f81b22134e65b2f802d&oe=60241F82",
  ),
];

List<ItemInstagram> itemLatestList = [
  ItemInstagram(
    title: "Black Widow",
    content: "At birth the Black Widow (aka Natasha Romanova) is given to the KGB, which grooms her to become its ultimate operative. When the U.S.S.R. breaks up, the government tries to kill her as the action moves to present-day New York, where she is a freelance operative.",
    img: "https://i.pinimg.com/originals/9b/97/87/9b9787fb5209b99c3207554a341e3f32.png",
  ),
];
