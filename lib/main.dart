import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app2222/routeGenerator.dart';
import 'package:flutter_app2222/screen/home.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:z_globle/g.dart' as gCo;

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    ///for transform
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.black,// navigation bar color
      statusBarColor: Colors.transparent, // status bar color
    ));
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    ///end transform
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: gCo.coNavigatorAppKey,
      theme: ThemeData(
        //primarySwatch: Colors.blue,
        primaryColor: HexColor('#005d86'),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
     // home: HomeScreen(),
      initialRoute: '/SplashOne',
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

