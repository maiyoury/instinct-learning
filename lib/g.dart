import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void logout(context)async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.remove('session');
  // Navigator.pushNamed(context, '/Login');
  Navigator.of(context).pushNamed('/Login');
}