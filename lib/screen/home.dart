import 'package:flutter/material.dart';
import 'package:flutter_app2222/widget/drawer.dart';
import 'package:flutter_app2222/widget/gridMenu.dart';
import 'package:flutter_app2222/widget/gridMenuAC.dart';
import 'package:z_globle/g_widget.dart' as gWidget;

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  Widget add() {
    return Container(
      child: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: null,
        tooltip: 'Add',
        child: Icon(Icons.add),
      ),
    );
  }

  Widget image() {
    return Container(
      child: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: null,
        tooltip: 'Image',
        child: Icon(Icons.image),
      ),
    );
  }

  Widget inbox() {
    return Container(
      child: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: null,
        tooltip: 'Inbox',
        child: Icon(Icons.inbox),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'សួស្តីឆ្នាំថ្មី',
          style: TextStyle(fontFamily: 'Khmer Os Freehand'),
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.notifications),
              onPressed: (){
                ///action button
              }),
          IconButton(
              icon: Icon(Icons.phone_in_talk),
              onPressed: () {
                ///action button
              }),
        ],
      ),
      drawer: CoDrawer(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            GridMenuHome(),
            //GridMenuAC(),
          ],
        ),
      ),
      //floatingActionButton: _buildFloatingActionButtonHome,
      //bottomNavigationBar: _buildButtonNavigationBarHome,
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  get _buildFloatingActionButtonHome {
    return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Transform(
              //transform: Matrix4.skewY(0.3)..rotateZ(12.0),
              transform: Matrix4.translationValues(0.0, 3.0, 0.0),
              child: add(),
            ),
            //toggle(),
          ],
        ),
    );
  }

  get _buildButtonNavigationBarHome {
    return Container(
      child: ClipRRect(
        // borderRadius: BorderRadius.only(
        //   topRight: Radius.circular(40),
        //   topLeft: Radius.circular(40),
        // ),
        child: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(40),
              topLeft: Radius.circular(40),
            ),
          ),
          child: BottomNavigationBar(
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
              BottomNavigationBarItem(icon: Icon(Icons.mail), title: Text('Message')),
              BottomNavigationBarItem(icon: Icon(Icons.school), title: Text('School')),
              BottomNavigationBarItem(icon: Icon(Icons.assignment_rounded), title: Text('News')),
            ],
            unselectedItemColor: Colors.grey,
            selectedItemColor: Colors.black,
            showUnselectedLabels: true,
          ),
        ),
      ),
    );
  }
}
