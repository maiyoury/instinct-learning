import 'package:flutter/material.dart';
import 'package:flutter_app2222/widget/listViewBuilder.dart';

class CardIconModel{
  String title;
  IconData icon;
  String image;
  GestureTapCallback tap;
  CardIconModel({this.icon, this.title, this.image, this.tap});
}



class CardAccountScreen extends StatefulWidget {
  @override
  _CardAccountScreenState createState() => _CardAccountScreenState();
}

class _CardAccountScreenState extends State<CardAccountScreen> {
  double _widthOfScreen;

  List<CardIconModel> cardIconModelBuild(){
    List<CardIconModel> list = [];
    list.add(new CardIconModel(image: 'assets/ABA/aba_Mastercard-Platinum_update.png',));
    list.add(new CardIconModel(image: 'assets/ABA/aba_mastercard_classic_debit.png',));

    return list;
  }



  @override
  Widget build(BuildContext context) {
    String url = 'assets/ABA/';
    _widthOfScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Card'),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Container(
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: Row(
                  children: [
                    Container(
                      child: Image.asset('assets/ABA/aba_Mastercard-Platinum_update.png')
                    ),
                    Container(
                      child: Image.asset('assets/ABA/aba_Mastercard-Platinum_update.png')
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  color: Colors.blueGrey,
                  height: 20,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
