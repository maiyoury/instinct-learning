import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:z_globle/g_widget.dart' as gWidget;



class PaymentIconModel{
  String title;
  IconData icon;
  String image;
  GestureTapCallback tap;
  PaymentIconModel({this.icon, this.title, this.image, this.tap});
}

class PaymentScreen extends StatefulWidget {
  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {

  List<PaymentIconModel> paymentIconModelBuild(){
    List<PaymentIconModel> list = [];
    list.add(new PaymentIconModel(title: 'ជ្រើសពីគំរូ', image: 'assets/icon/a_money.png',));
    list.add(new PaymentIconModel(title: 'បញ្ចូលលុយទូរស័ព្ទ', image: 'assets/icon/a_call.png',));
    list.add(new PaymentIconModel(title: 'អ៊ីនធឺណេត & ទូរស្សន៍', image: 'assets/icon/a_wifi.png',));
    list.add(new PaymentIconModel(title: 'ទឹកភ្លើង និង សំរាម', image: 'assets/icon/a_conect.png',));
    list.add(new PaymentIconModel(title: 'សេវាហិរញ្ញវត្ថុ', image: 'assets/icon/a_pay.png',));
    list.add(new PaymentIconModel(title: 'ការសិក្សា', image: 'assets/icon/a_school.png',));
    list.add(new PaymentIconModel(title: 'កម្សាន្ត', image: 'assets/icon/a_game.png',));
    list.add(new PaymentIconModel(title: 'សេវាសាធារណៈ', image: 'assets/icon/a_other.png',));

    return list;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ABA Payment"),
        actions: [
          IconButton(icon: Icon(Icons.search_outlined, color: Colors.white,), onPressed: null),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          color: HexColor('#e7eff2'),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 120,
                  child: Stack(
                    children: [
                      Positioned(
                        bottom: -30,
                        right: -40,
                        child: Container(
                          height: 140,
                          width: 140,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            //border: Border.all(width: 1, color: Colors.white),
                            //color: Colors.lightBlue[200]
                          ),
                          child: Center(child: Image.asset('assets/icon/moneytransfer.png', color: HexColor('#f7a1a0'),)),
                        ),
                      ),
                      Positioned(
                        bottom: 30,
                        right: 150,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100000),
                            //border: Border.all(width: 1, color: Colors.white),
                            //color: Colors.lightBlue[200]
                          ),
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('ទូទាត់ប្រាក់', style: TextStyle(fontSize: 20, color: Colors.white),),
                                SizedBox(height: 5,),
                                Text('ប្រើប្រាស់សេវាកម្ម ABA ដើម្បិបញ្ចូលលុយទូរស័ព្ទ', style: TextStyle(fontSize: 10, color: Colors.white),),
                                SizedBox(height: 3,),
                                Text('បង់វិក្កយបត្រ ភ្លើង ឬថ្លៃសាលាកូនរបស់អ្នកបាន', style: TextStyle(fontSize: 10, color: Colors.white),),
                                SizedBox(height: 3,),
                                Text('យ៉ាងងាយស្រូល គ្រប់ពេលវេលា និងពីគ្រប់ទីកន្លែង។', style: TextStyle(fontSize: 10, color: Colors.white),),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  color: HexColor('#ee5351'),
                ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  //color: Colors.blueGrey,
                  child: Column(
                    children: List.generate(paymentIconModelBuild().length, (index){
                      return InkWell(
                        onTap: (){
                          paymentIconModelBuild()[index].tap();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white,
                          ),
                          margin: EdgeInsets.only(bottom: 10),
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                          child: Column(
                            children: [
                              ListTile(
                                leading: paymentIconModelBuild()[index].image == null ? Icon(paymentIconModelBuild()[index].icon, color: Colors.white,size: 50,)
                                    : Image.asset(paymentIconModelBuild()[index].image, width: 50,),
                                title:  Text(paymentIconModelBuild()[index].title, style: TextStyle(color: Colors.black87),),
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
