import 'package:flutter/material.dart';
import 'package:flutter_app2222/screen/home.dart';
import 'package:flutter_app2222/screen/notificationScreen.dart';
import 'package:flutter_app2222/screen/setting.dart';
import 'package:z_globle/model/bottomNavigationBarModel.dart';
import 'package:z_globle/g_widget.dart' as gWidget;

class TabButtonCustom extends StatefulWidget {
  @override
  _TabButtonCustomState createState() => _TabButtonCustomState();
}

class _TabButtonCustomState extends State<TabButtonCustom> {

  int currentTap = 1;
  List<CoBottomNavigationBarModel> getBuildTap(){
    List<CoBottomNavigationBarModel> list = [];
    list.add(new CoBottomNavigationBarModel(icon: Icons.home_outlined, title: 'Home'));
    list.add(new CoBottomNavigationBarModel(icon: Icons.notifications_none, title: 'Notification'));
    list.add(new CoBottomNavigationBarModel(icon: Icons.settings, title: 'Setting'));
    return list;
  }

  //tab to screen
  Widget buildTapBody(int current){
    switch (current) {
      case 1:
        return new HomeScreen();
        break;
      case 2:
        return new NotificationScreen();
        break;
      case 3:
        return new SettingScreen();
        break;
      default:
        return HomeScreen();
        break;
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        return null;
      },
      child: Scaffold(
        body: buildTapBody(currentTap),
        bottomNavigationBar: gWidget.CoBottomNavigationBar(
          currentTap: currentTap,
          tapBuild: getBuildTap(),
          onChange: (val){
            setState(() {
              currentTap = val;
            });
          },
        ),
      ),
    );
  }
}
