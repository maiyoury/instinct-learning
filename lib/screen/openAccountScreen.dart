import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:z_globle/g_widget.dart' as gWidget;

class OpenAccountIconModel{
  String title;
  IconData icon;
  String image;
  GestureTapCallback tap;
  OpenAccountIconModel({this.icon, this.title, this.image, this.tap});
}
String imgUrl = 'assets/ABA/';

class OpenAccountScreen extends StatefulWidget {

  @override
  _OpenAccountScreenState createState() => _OpenAccountScreenState();
}

class _OpenAccountScreenState extends State<OpenAccountScreen> {

  List<OpenAccountIconModel> openAccountIconModelBuild(){
    List<OpenAccountIconModel> list = [];
    list.add(new OpenAccountIconModel(title: 'ជ្រើសពីគំរូ', image: 'assets/icon/a_money.png',));
    list.add(new OpenAccountIconModel(title: 'បញ្ចូលលុយទូរស័ព្ទ', image: 'assets/icon/a_call.png',));
    list.add(new OpenAccountIconModel(title: 'អ៊ីនធឺណេត & ទូរស្សន៍', image: 'assets/icon/a_wifi.png',));
    list.add(new OpenAccountIconModel(title: 'ទឹកភ្លើង និង សំរាម', image: 'assets/icon/a_conect.png',));
    list.add(new OpenAccountIconModel(title: 'សេវាហិរញ្ញវត្ថុ', image: 'assets/icon/a_pay.png',));
    list.add(new OpenAccountIconModel(title: 'ការសិក្សា', image: 'assets/icon/a_school.png',));
    list.add(new OpenAccountIconModel(title: 'កម្សាន្ត', image: 'assets/icon/a_game.png',));
    list.add(new OpenAccountIconModel(title: 'សេវាសាធារណៈ', image: 'assets/icon/a_other.png',));

    return list;
  }


  List list = [
    {
      'title' : 'លេខគណនីពិសេស',
      'date' : 'ថ្ងៃនេះ ម៉ោង 13:15',
      'content' : 'បើកគណនី ABA ជាមួយលេខពិសេស ងាយស្រួលចាំ។ លេខពិសេសនេះ អាចជាថ្ងៃខែឆ្នាំកំណើត ផ្លាកលេខយានយន្ត ឬសេខផ្សេៗដែពិសេសសម្រប់អ្នក។',
      'img' : '${imgUrl}aba_OTT.png',
    },
    {
      'title' : 'គណនីបញ្ចើមានកាលកំណត់',
      'date' : 'ថ្ងៃនេះ ម៉ោង 09:52',
      'content' : 'ជ្រើសយកបញ្ញើកាលកំណត់ ដើម្បិទទួលបានផលចំណេញខ្ពស់ពីការសន្សំរបស់លោកអ្នកជាមួយនឹងអត្រាការប្រាក់ជាទីពេញចិត្តទាំងប្រាក់កុល្លាអាមរិច និងប្រាក់រៀល។',
      'img' : '${imgUrl}aba_pay_icon.png',
    },
    {
      'title' : 'គណនីបញ្ចើមានកាលកំណត់',
      'date' : 'ថ្ងៃនេះ ម៉ោង 09:37',
      'content' : 'គណនីសន្សំពេញនិយមប៉ផុតពីធនាគារយើងខ្ញុំ ដែលជួយអោយលោកអ្នកសម្រេចគោលដៅសន្សំជាមួយការប្រាក់ជាទីពេញចិត្ត និងលក្ខណៈពិសេសៗជាច្រើនទៀត។',
      'img' : '${imgUrl}aba_protection.png',
    },
    {
      'title' : 'គណនីបញ្ចើមានកាលកំណត់',
      'date' : 'ថ្ងៃនេះ ម៉ោង 13:15',
      'content' : 'ជា​រៀងរាល់សប្ដាហ៍​ Billboard តែង​បង្ហាញ​ពី​លទ្ធផល​បទ​ចម្រៀង​ដែល​ជាប់​លើតារាង​ «World Digital Song Sales» របស់ពួកគេ ជា​ការ​បញ្ជាក់​ឲ្យ​ច្បាស់​ថា​បទ​ណា​ខ្លះ​ដែល​ពេញ​និយម​ និង​លក់​ដាច់ជាងគេ​នៅ​ក្នុង​សហរដ្ឋអាមេរិក។',
      'img' : '${imgUrl}E_Cash.png',
    },
  ];

  bool descTextShowFlag = false;

  @override
  Widget build(BuildContext context) {

    String path = 'assets/icon/';
    return Scaffold(
      appBar: AppBar(
        title: Text('ABA បើកគណនី'),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.blueGrey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  height: 120,
                  width: MediaQuery.of(context).size.width * 2,
                  color: Theme.of(context).primaryColor,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 20,),
                      Image.asset('assets/ABA/Account_Opening.png', width: 50, height: 50,),
                      SizedBox(height: 5,),
                      Text('បើកគណនីថ្មី', style: TextStyle(color: Colors.white),)
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: Column(
                    children: List.generate(list.length, (index){
                      return Container(
                        decoration: BoxDecoration(
                           borderRadius: BorderRadius.circular(5),
                           color: Colors.white,
                        ),
                        margin: EdgeInsets.only(bottom: 10),
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                    height: 50,
                                    width: 50,
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(100),
                                        child: Image.asset(list[index]['img'])
                                    )
                                ),
                                SizedBox(width: 15,),
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '${list[index]['title']}',
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          softWrap: false,
                                          style: TextStyle(fontWeight: FontWeight.bold),),
                                        SizedBox(height: 2,),
                                        Text('${list[index]['content']}',  maxLines: descTextShowFlag ? 8 : 3,textAlign: TextAlign.start, style: TextStyle(color: Colors.grey[900], fontSize: 12),),
                                        SizedBox(height: 10,),
                                        InkWell(
                                          onTap: (){
                                            setState(() {
                                              descTextShowFlag = !descTextShowFlag;
                                            });
                                          },
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: <Widget>[
                                              descTextShowFlag ? Text("តិចជាង",style: TextStyle(color: Colors.blueGrey),) :  Text("ច្រើនជាង",style: TextStyle(color: Colors.blueGrey))
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    }),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
