import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app2222/item.dart';
import 'package:flutter_app2222/widget/listViewBuilder.dart';

class NetflixScreen extends StatefulWidget {
  @override
  _NetflixScreenState createState() => _NetflixScreenState();
}

class _NetflixScreenState extends State<NetflixScreen> {
  double _widthOfScreen;

  @override
  Widget build(BuildContext context) {
    _widthOfScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      body: _buildBody
    );
  }

  get _buildBody {
    return Container(
      color: Colors.black87,
      child: ListView(
        children: [
          _buildBanner,
          _buildStoryRow,
          _buildPreviewRow,
        ],
      ),
    );
  }

  get _buildBanner {
    return Container(
      child: Stack(
        children: [
          _buildBackImage,
          _buildBottomShadow,
          _buildTopShadow,
          _buildForeground,
        ],
      ),
    );
  }

  get _buildBackImage {
    String imgUrl = 'https://cdn.vox-cdn.com/thumbor/2SLqPJefnX7gGOYu7bsTNculpo8=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/19891901/D_Unit_22349_R.jpg';
    return Container(
      height: 500,
      child: Image.network(imgUrl, fit: BoxFit.cover,),
    );
  }

  get _buildBottomShadow {
    return Positioned(
      bottom: 0,
      child: Container(
        height: 250,
        width: _widthOfScreen,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                stops: [0.10, 0.25, 0.85],
                colors: [
                  Colors.black,
                  Colors.black87,
                  Colors.black12.withOpacity(0.0),
                ]
            )
        ),
      ),
    );
  }

  get _buildTopShadow {
    return Positioned(
      top: 0,
      child: Container(
        height: 250,
        width: _widthOfScreen,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.10, 0.25, 0.85],
                colors: [
                  Colors.black,
                  Colors.black87,
                  Colors.black12.withOpacity(0.0),
                ]
            )
        ),
      ),
    );
  }

  get _buildForeground {
    String netflix = 'https://assets.stickpng.com/images/580b57fcd9996e24bc43c529.png';
    return Positioned(
      bottom: 20,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 70,
              width: 200,
              //child: Image.network(netflix, fit: BoxFit.cover,),
            ),
            Text('THE\nMIDNIGHT\nSKY', style: TextStyle(fontSize: 50, color: Colors.white), textAlign: TextAlign.center,),
            _buildForegroundContent,
          ],
        ),
      ),
    );
  }

  get __buildForegroundTopContent{
    return Positioned(
      top: 0,
      child: Container(
        height: 50,
        width: _widthOfScreen,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _buildMyListIcon,
            _buildPlayButton,
            _buildInfoIcon,
          ],
        ),
      ),
    );
  }

  get _buildForegroundContent {
    return Container(
      height: 50,
      width: _widthOfScreen,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _buildMyListIcon,
          _buildPlayButton,
          _buildInfoIcon,
        ],
      ),
    );
  }

  get _buildMyListIcon {
    return InkWell(
      onTap: () {
        print('My List checked');
      },
      child: Container(
        height: 50,
        width: 50,
        child: Column(
          children: [
            Icon(Icons.add, color: Colors.white,),
            Text('My List', style: TextStyle(color: Colors.white),)
          ],
        ),
      ),
    );
  }

  get _buildPlayButton {
    return Container(
      height: 30,
      width: 90,
      child: RaisedButton(
        child: Row(
          children: [
            Icon(Icons.play_circle_fill),
            Text('Play'),
          ],
        ),
        onPressed: () {
          print('Play Button checked');
        },
      ),
    );
  }

  get _buildInfoIcon {
    return InkWell(
      onTap: () {
        print('Info clicked');
      },
      child: Container(
        height: 50,
        width: 50,
        child: Column(
          children: [
            Icon(Icons.info_outline, color: Colors.white,),
            Text('Info', style: TextStyle(color: Colors.white),),
          ],
        ),
      ),
    );
  }


  get _buildStoryRow {
    return Container(
      height: 250,
      width: _widthOfScreen,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: itemStoryList.length,
        itemBuilder: (context, index){
          return _buildListItem(itemStoryList[index]);
        }
      ),
    );
  }

  get _buildPreviewRow{
    return Container(
      height: 250,
      width: _widthOfScreen,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: itemList.length,
        itemBuilder: (context, index){
          return _buildPreviewListItem(itemList[index]);
        }
      ),
    );
  }



  _buildListItem(Item item){
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 170,
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            image: DecorationImage(
              image: NetworkImage(item.img),
              fit: BoxFit.cover,
            ),
          ),
        ),
        InkWell(
          onTap: (){
            print('clicked ${item.title}');
            Navigator.of(context).pushNamed('/SingleNetflixScreen');
          },
          child: Container(
            height: 80,
            width: 80,
            alignment: Alignment.center,
            child: Icon(Icons.play_circle_fill, color: Colors.white60, size: 70,),
          ),
        ),
        Positioned(
            top: 20,
            child: Container(
              height: 30,
              width: 150,
              color: Colors.white60,
              alignment: Alignment.center,
              child: Text(item.title, overflow: TextOverflow.ellipsis,),
            )
        )
      ],
    );
  }
  _buildPreviewListItem(Item item){
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 170,
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(item.img),fit: BoxFit.cover),
          ),
        ),
        Positioned(
          bottom: 20,
          child: Container(
            height: 30,
            width: 150,
            alignment: Alignment.center,
            child: Text(item.title, overflow: TextOverflow.ellipsis,),
          ),
        )
      ],
    );
  }

}
