import 'package:flutter/material.dart';
import 'package:flutter_app2222/item.dart';



class SingleNetflixScreen extends StatefulWidget {
  
  final Item item;
  SingleNetflixScreen({this.item});
  @override
  _SingleNetflixScreenState createState() => _SingleNetflixScreenState();
}

class _SingleNetflixScreenState extends State<SingleNetflixScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar,
      body: _buildBody,
    );
  }
  get _buildAppBar{
    return AppBar(
      title: Text('Detail'),
    );
  }
  
  get _buildBody{
    return Container(
      child: Column(
        children: [
          Container(
            child: Image.network(widget.item.img, fit: BoxFit.cover,),
          ),
          Expanded(
            child: Container(
              child: Text('${widget.item.title}'),
            ),
          ),
        ],
      ),
    );
  }
}
