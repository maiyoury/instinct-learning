import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:z_globle/g_form.dart' as gForm;
import 'package:z_globle/g_form.dart' as gCo;
import 'package:z_globle/g_widget.dart' as gWidget;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: (){
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: _buildFormLoginTextField,
      ),
      bottomNavigationBar: _buildButtonNavigationBar,
    );
  }

  get _buildTextField {
    return Container(
      //alignment: Alignment.center,
      color: Colors.green,
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          gForm.coTextField(
            text: 'Name',
          ),
          SizedBox(
            height: 20,
          ),
          gForm.coTextField(
            text: 'Password',
          ),
          SizedBox(
            height: 20,
          ),
          gForm.coButton(
            text: 'Submit',
          ),
        ],
      ),
    );
  }

  get _buildCenterTextField {
    return Center(
      child: Container(
        color: Colors.green,
        child: Padding(
          padding: EdgeInsets.all(36.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 155.0,
                child: Image.asset(
                  "assets/icon/Logo_Messenger.png",
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(height: 20,),
              gForm.coTextField(
                text: 'បំពេញឈ្មោះ',
              ),
              SizedBox(height: 20,),
              gForm.coTextField(
                text: 'បំពេញលេខសំងាត់',
              ),
              ListTile(
                title: Text('Remember me'),
                leading: Checkbox(
                  value: false,
                  onChanged: (value) {
                    setState(() {});
                  },
                ),
              ),
              SizedBox(height: 20,),
              gForm.coButton(
                text: 'ចូលគណនី',
                textStyle: TextStyle(fontFamily: 'Khmer OS Freehand'),
                onPressed: () {
                  Navigator.of(context).pushNamed('/TabButtonCustom');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  get _buildStackPosition {
    return Container(
      child: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          Positioned(
            top: -200,
            left: -90,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100000),
                color: HexColor('#155cac'),
                //border: Border.all(width: 2)
              ),
              height: 450,
              width: 450,
              //child: Center(child: Text('Positioned')),
            ),
          ),
          Positioned(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100000),
                //border: Border.all(width: 2, color: Colors.white),
                //color: Colors.white
              ),
              height: 250,
              width: 250,
              //child: Center(child: Text('Container')),
            ),
          ),
          Positioned(
            top: -20,
            left: 50,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100000),
                //border: Border.all(width: 1, color: Colors.white),
                //color: Colors.lightBlue[200]
              ),
              height: 250,
              width: 250,
              //child: Center(child: Text('dddd')),
              child: Image.network('https://www.searchpng.com/wp-content/uploads/2019/03/Facebook-Icon-PNG-715x715.png'),
            ),
          ),
          Positioned(
            top: 10,
            right: -10,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100000),
                //border: Border.all(width: 1, color: Colors.white),
                //color: Colors.lightBlue[200]
              ),
              height: 200,
              width: 200,
              //child: Center(child: Text('ttttt')),
            ),
          ),
        ],
      )
    );
  }

  get _buildButtonNavigationBar {
    return Container(
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.only(
             topRight: Radius.circular(40),
             topLeft: Radius.circular(40),
            //bottomLeft: Radius.circular(40),
            //bottomRight: Radius.circular(40),
          ),
        ),
        width: double.infinity,
        height: 40,
      ),
    );
  }

  get _buildFormLoginTextField {
    return Container(
      color: Colors.grey[200],
      //padding: EdgeInsets.all(10),
      child: ListView(
        children: <Widget>[
          Container(
            child: _buildStackPosition,
            //color: Colors.red,
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: TextField(
              controller: nameController,
              decoration: InputDecoration(
                suffixIcon: Icon(Icons.person),
                border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                labelText: 'User Name',
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: TextField(
              obscureText: true,
              //controller: passwordController,
              decoration: InputDecoration(
                suffixIcon: Icon(Icons.lock),
                border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                labelText: 'Password',
              ),
            ),
          ),
          FlatButton(
            onPressed: (){
              //forgot password screen
            },
            //textColor: Colors.blue,
            textColor: Theme.of(context).primaryColor,
            child: Text('Forgot Password'),
          ),
          Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: RaisedButton(
                textColor: Colors.white,
                color: Theme.of(context).primaryColor,
                child: Text('Login'),
                onPressed: () {
                  //Navigator.of(context).pushNamed('/HomeScreen');
                  Navigator.of(context).pushNamed('/TabButtonCustom');
                  print(nameController.text);
                  print(passwordController.text);
                },
              ),
          ),
          Container(
              child: Row(
                children: <Widget>[
                  Text('Does not have account?'),
                  FlatButton(
                    textColor: Theme.of(context).primaryColor,
                    child: Text(
                      'Sign in',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      //sign up screen
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
          ),
        ],
      ),
    );
  }
}
