import 'package:flutter/material.dart';

class LearnInstinctFromScreen extends StatefulWidget {
  @override
  _LearnInstinctFromScreenState createState() => _LearnInstinctFromScreenState();
}

class _LearnInstinctFromScreenState extends State<LearnInstinctFromScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar,
      body: _buildBody,
      drawer: _buildDrawer,
    );
  }

  String _title = 'Hello';
  bool _dark = false;
  bool _hide = true;
  get _buildAppBar{
    return AppBar(
      backgroundColor: _dark == true ? Colors.blueGrey : Colors.pink,
      centerTitle: true,
      title: Text(_title),
      actions: [
        IconButton(icon: Icon(Icons.settings), onPressed: (){
          setState(() {
            _title = 'Hi';
            _dark =!_dark;
          });
        }),
      ],
    );
  }

  get _buildDrawer{
    return Drawer(
      elevation: 0.0,
    );
  }

  get _buildBody{
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'User Name',
                suffixIcon: Icon(Icons.person)
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: TextField(
              obscureText: _hide,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
                suffixIcon: IconButton(
                  icon: Icon(_hide ? Icons.visibility : Icons.visibility_off),
                  onPressed: (){
                    setState(() {
                      _hide =! _hide;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

}
