import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app2222/item.dart';
import 'package:flutter_app2222/screen/home.dart';
import 'package:flutter_app2222/instagramClone/screen/instagramHomeScreen.dart';
import 'package:flutter_app2222/screen/learnInstinctFormScreen.dart';
import 'package:flutter_app2222/screen/login.dart';
import 'package:flutter_app2222/screen/netflixScreen.dart';
import 'package:flutter_app2222/screen/notificationScreen.dart';
import 'package:flutter_app2222/screen/openAccountScreen.dart';
import 'package:flutter_app2222/screen/paymentScreen.dart';
import 'package:flutter_app2222/screen/setting.dart';
import 'package:flutter_app2222/screen/singleNetflixScreen.dart';
import 'package:flutter_app2222/screen/splash/splashOne.dart';
import 'package:flutter_app2222/screen/tapScreen.dart';
import 'package:flutter_app2222/widget/drawer.dart';
import 'package:flutter_app2222/widget/gridMenu.dart';
import 'package:flutter_app2222/screen/cardAccountScreen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
      case '/TabButtonCustom':
        return CupertinoPageRoute(
          builder: (_) => TabButtonCustom(),
        );
      case '/HomeScreen':
        return CupertinoPageRoute(
          builder: (_) => HomeScreen(),
        );
      case '/CoDrawer':
        return CupertinoPageRoute(
          builder: (_) => CoDrawer(),
        );
      case '/LoginScreen':
        return CupertinoPageRoute(
          builder: (_) => LoginScreen(),
        );
      case '/NotificationScreen':
        return CupertinoPageRoute(
          builder: (_) => NotificationScreen(),
        );
      case '/SettingScreen':
        return CupertinoPageRoute(
          builder: (_) => SettingScreen(),
        );
      case '/GridMenu':
        return CupertinoPageRoute(
          builder: (_) => GridMenuHome(),
        );
      case '/NetflixScreen':
        return CupertinoPageRoute(
          builder: (_) => NetflixScreen(),
        );
      case '/PaymentScreen':
        return CupertinoPageRoute(
          builder: (_) => PaymentScreen(),
        );
      case '/OpenAccountScreen':
        return CupertinoPageRoute(
          builder: (_) => OpenAccountScreen(),
        );
      case '/CardAccountScreen':
        return CupertinoPageRoute(
          builder: (_) => CardAccountScreen(),
        );
      case '/LearnInstinctFromScreen':
        return CupertinoPageRoute(
          builder: (_) => LearnInstinctFromScreen(),
        );
      case '/SingleNetflixScreen':
        return CupertinoPageRoute(
          builder: (_) => SingleNetflixScreen(item: args),
        );
      case '/InstagramHomeScreen':
        return CupertinoPageRoute(
          builder: (_) => InstagramHomeScreen(),
        );
      case '/SplashOne':
        return CupertinoPageRoute(
          builder: (_) => SplashOne(),
        );
    }
  }
}