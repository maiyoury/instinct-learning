class Item {
  String title, img, body;

  Item({this.title, this.img, this.body});
}

List<Item> itemList = [
  Item(
    title: "The Eternals",
    body:
        "The saga of the Eternals, a race of immortal beings who lived on Earth and shaped its history and civilisations.",
    img:
        "https://thedirect.s3.amazonaws.com/media/article_full/postereternals_zisRk9T_UxgXGRv.jpg",
  ),
  Item(
    title: "Black Widow",
    body:
    "At birth the Black Widow (aka Natasha Romanova) is given to the KGB, which grooms her to become its ultimate operative. When the U.S.S.R. breaks up, the government tries to kill her as the action moves to present-day New York, where she is a freelance operative.",
    img:
    "https://terrigen-cdn-dev.marvel.com/content/prod/1x/blackwidow_lob_crd_04.jpg",
  ),
  Item(
    title: "Morbius",
    body:
    "Biochemist Michael Morbius tries to cure himself of a rare blood disease, but when his experiment goes wrong, he inadvertently infects himself with a form of vampirism instead.",
    img:
    "https://i.ytimg.com/vi/bVdscenkxjY/maxresdefault.jpg",
  ),
  Item(
    title: "Shang-Chi and the Legend of the Ten Rings",
    body:
    "Shang-Chi is a master of numerous unarmed and weaponry-based wushu styles, including the use of the gun, nunchaku, and jian.",
    img:
    "https://cnet2.cbsistatic.com/img/XLEGI6jXPwM5Qc7pENx4GJF_apg=/1200x630/2017/11/30/f6489aba-2ede-4120-9897-38ee5219a43b/shang-chi-comics.jpg",
  ),
  Item(
    title: "Raya and the Last Dragon",
    body:
    "Long ago, in the fantasy world of Kumandra, humans and dragons lived together in harmony. However, when sinister monsters known as the Druun threatened the land, the dragons sacrificed themselves to save humanity. Now, 500 years later, those same monsters have returned, and it's up to a lone warrior to track down the last dragon and stop the Druun for good.",
    img:
    "https://cdn.vox-cdn.com/thumbor/DtSix7gfPqjT_6z3iEpKbgVV-n8=/0x0:6080x2546/1200x800/filters:focal(2554x787:3526x1759)/cdn.vox-cdn.com/uploads/chorus_image/image/68493981/RAYA_ONLINE_USE_First_Look_CG_Final_RGB__1_.0.jpg",
  ),
  Item(
    title: "Ghostbusters: Afterlife",
    body:
    "When a single mother and her two children move to a new town, they soon discover that they have a connection to the original Ghostbusters and their grandfather's secret legacy.",
    img:
    "https://deadline.com/wp-content/uploads/2020/03/ghostbusters-afterlife-e1585615934205.jpg",
  ),
  Item(
    title: "Top Gun 2",
    body:
    "Pete \"Maverick\" Mitchell keeps pushing the envelope after years of service as one of the Navy's top aviators. He must soon confront the past while training a new squad of graduates for a dangerous mission that demands the ultimate sacrifice.",
    img:
    "https://i.ytimg.com/vi/zs_UrYH_rhs/maxresdefault.jpg",
  ),
  Item(
    title: "Coming To America 2",
    body:
    "Coming 2 America is an upcoming American comedy film directed by Craig Brewer, from a screenplay by Kenya Barris, Barry W. Blaustein, and David Sheffield, and a story by Blaustein, Sheffield, and Justin Kanew, based on characters created by Eddie Murphy.",
    img:
    "https://www.indiewire.com/wp-content/uploads/2020/12/coming-2-america.png?w=780",
  ),
];

List<Item> itemStoryList = [
  Item(
    title: "Top Gun 2",
    body:
    "Pete \"Maverick\" Mitchell keeps pushing the envelope after years of service as one of the Navy's top aviators. He must soon confront the past while training a new squad of graduates for a dangerous mission that demands the ultimate sacrifice.",
    img:
    "https://i.ytimg.com/vi/zs_UrYH_rhs/maxresdefault.jpg",
  ),
  Item(
    title: "Black Widow",
    body:
    "At birth the Black Widow (aka Natasha Romanova) is given to the KGB, which grooms her to become its ultimate operative. When the U.S.S.R. breaks up, the government tries to kill her as the action moves to present-day New York, where she is a freelance operative.",
    img:
    "https://terrigen-cdn-dev.marvel.com/content/prod/1x/blackwidow_lob_crd_04.jpg",
  ),

  Item(
    title: "The Eternals",
    body:
    "The saga of the Eternals, a race of immortal beings who lived on Earth and shaped its history and civilisations.",
    img:
    "https://thedirect.s3.amazonaws.com/media/article_full/postereternals_zisRk9T_UxgXGRv.jpg",
  ),
  Item(
    title: "Coming To America 2",
    body:
    "Coming 2 America is an upcoming American comedy film directed by Craig Brewer, from a screenplay by Kenya Barris, Barry W. Blaustein, and David Sheffield, and a story by Blaustein, Sheffield, and Justin Kanew, based on characters created by Eddie Murphy.",
    img:
    "https://www.indiewire.com/wp-content/uploads/2020/12/coming-2-america.png?w=780",
  ),
  Item(
    title: "Shang-Chi and the Legend of the Ten Rings",
    body:
    "Shang-Chi is a master of numerous unarmed and weaponry-based wushu styles, including the use of the gun, nunchaku, and jian.",
    img:
    "https://cnet2.cbsistatic.com/img/XLEGI6jXPwM5Qc7pENx4GJF_apg=/1200x630/2017/11/30/f6489aba-2ede-4120-9897-38ee5219a43b/shang-chi-comics.jpg",
  ),
  Item(
    title: "Raya and the Last Dragon",
    body:
    "Long ago, in the fantasy world of Kumandra, humans and dragons lived together in harmony. However, when sinister monsters known as the Druun threatened the land, the dragons sacrificed themselves to save humanity. Now, 500 years later, those same monsters have returned, and it's up to a lone warrior to track down the last dragon and stop the Druun for good.",
    img:
    "https://cdn.vox-cdn.com/thumbor/DtSix7gfPqjT_6z3iEpKbgVV-n8=/0x0:6080x2546/1200x800/filters:focal(2554x787:3526x1759)/cdn.vox-cdn.com/uploads/chorus_image/image/68493981/RAYA_ONLINE_USE_First_Look_CG_Final_RGB__1_.0.jpg",
  ),
  Item(
    title: "Morbius",
    body:
    "Biochemist Michael Morbius tries to cure himself of a rare blood disease, but when his experiment goes wrong, he inadvertently infects himself with a form of vampirism instead.",
    img:
    "https://i.ytimg.com/vi/bVdscenkxjY/maxresdefault.jpg",
  ),
  Item(
    title: "Ghostbusters: Afterlife",
    body:
    "When a single mother and her two children move to a new town, they soon discover that they have a connection to the original Ghostbusters and their grandfather's secret legacy.",
    img:
    "https://deadline.com/wp-content/uploads/2020/03/ghostbusters-afterlife-e1585615934205.jpg",
  ),
];

List<Item> itemLatestList = [
  Item(
    title: "Black Widow",
    body:
    "At birth the Black Widow (aka Natasha Romanova) is given to the KGB, which grooms her to become its ultimate operative. When the U.S.S.R. breaks up, the government tries to kill her as the action moves to present-day New York, where she is a freelance operative.",
    img:
    "https://terrigen-cdn-dev.marvel.com/content/prod/1x/blackwidow_lob_crd_04.jpg",
  ),
  Item(
    title: "Top Gun 2",
    body:
    "Pete \"Maverick\" Mitchell keeps pushing the envelope after years of service as one of the Navy's top aviators. He must soon confront the past while training a new squad of graduates for a dangerous mission that demands the ultimate sacrifice.",
    img:
    "https://i.ytimg.com/vi/zs_UrYH_rhs/maxresdefault.jpg",
  ),



  Item(
    title: "Coming To America 2",
    body:
    "Coming 2 America is an upcoming American comedy film directed by Craig Brewer, from a screenplay by Kenya Barris, Barry W. Blaustein, and David Sheffield, and a story by Blaustein, Sheffield, and Justin Kanew, based on characters created by Eddie Murphy.",
    img:
    "https://www.indiewire.com/wp-content/uploads/2020/12/coming-2-america.png?w=780",
  ),
  Item(
    title: "The Eternals",
    body:
    "The saga of the Eternals, a race of immortal beings who lived on Earth and shaped its history and civilisations.",
    img:
    "https://thedirect.s3.amazonaws.com/media/article_full/postereternals_zisRk9T_UxgXGRv.jpg",
  ),

  Item(
    title: "Raya and the Last Dragon",
    body:
    "Long ago, in the fantasy world of Kumandra, humans and dragons lived together in harmony. However, when sinister monsters known as the Druun threatened the land, the dragons sacrificed themselves to save humanity. Now, 500 years later, those same monsters have returned, and it's up to a lone warrior to track down the last dragon and stop the Druun for good.",
    img:
    "https://cdn.vox-cdn.com/thumbor/DtSix7gfPqjT_6z3iEpKbgVV-n8=/0x0:6080x2546/1200x800/filters:focal(2554x787:3526x1759)/cdn.vox-cdn.com/uploads/chorus_image/image/68493981/RAYA_ONLINE_USE_First_Look_CG_Final_RGB__1_.0.jpg",
  ),
  Item(
    title: "Morbius",
    body:
    "Biochemist Michael Morbius tries to cure himself of a rare blood disease, but when his experiment goes wrong, he inadvertently infects himself with a form of vampirism instead.",
    img:
    "https://i.ytimg.com/vi/bVdscenkxjY/maxresdefault.jpg",
  ),
  Item(
    title: "Shang-Chi and the Legend of the Ten Rings",
    body:
    "Shang-Chi is a master of numerous unarmed and weaponry-based wushu styles, including the use of the gun, nunchaku, and jian.",
    img:
    "https://cnet2.cbsistatic.com/img/XLEGI6jXPwM5Qc7pENx4GJF_apg=/1200x630/2017/11/30/f6489aba-2ede-4120-9897-38ee5219a43b/shang-chi-comics.jpg",
  ),
  Item(
    title: "Ghostbusters: Afterlife",
    body:
    "When a single mother and her two children move to a new town, they soon discover that they have a connection to the original Ghostbusters and their grandfather's secret legacy.",
    img:
    "https://deadline.com/wp-content/uploads/2020/03/ghostbusters-afterlife-e1585615934205.jpg",
  ),
];
