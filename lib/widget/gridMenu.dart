import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:z_globle/g_widget.dart' as gWidget;


class HomeIconModel{
  String title;
  IconData icon;
  String image;
  GestureTapCallback tap;
  HomeIconModel({this.icon, this.title, this.image, this.tap});
}

class GridMenuHome extends StatefulWidget {
  @override
  _GridMenuHomeState createState() => _GridMenuHomeState();
}

class _GridMenuHomeState extends State<GridMenuHome> {

  ///model icon
  List<HomeIconModel> homeIconModelBuild(){
    List<HomeIconModel> list = [];
    list.add(new HomeIconModel(title: 'គណនី', image: 'assets/ABA/Check_Balance.png',
        tap: (){
          showDialog(context: context,
              builder: _buildComingSoon
          );
    }));
    list.add(new HomeIconModel(title: 'កាត', icon: Icons.person, image: 'assets/ABA/aba_othercard.png',
        tap: (){
          Navigator.of(context).pushNamed('/CardAccountScreen');
        }
        ));
    list.add(new HomeIconModel(title: 'ទូទាត់ទឹកប្រាក់', icon: Icons.person, image: 'assets/ABA/Pay_Bills.png',
    tap: (){
      Navigator.of(context).pushNamed('/PaymentScreen');
    }
    ));
    list.add(new HomeIconModel(title: 'បើកគណនីថ្មី', icon: Icons.person, image: 'assets/ABA/Finger_Print.png',
    tap: () {
      Navigator.of(context).pushNamed('/OpenAccountScreen');
    }));
    list.add(new HomeIconModel(title: 'ផ្ទេរប្រាក់ទៅ ATM', icon: Icons.person, image: 'assets/ABA/E_Cash.png',
    tap: (){
      showDialog(context: context,
          builder: _buildComingSoon
      );
    }));
    list.add(new HomeIconModel(title: 'ផ្ទេរប្រាក់', icon: Icons.person, image: 'assets/ABA/Template.png',
    tap: (){
      showDialog(context: context,
          builder: _buildComingSoon
      );
    }
    ));
    list.add(new HomeIconModel(title: 'ស្កេន QR', icon: Icons.person, image: 'assets/ABA/aba_protection.png',
    tap: (){
      showDialog(context: context,
          builder: _buildComingSoon
      );
    }
    ));
    list.add(new HomeIconModel(title: 'កម្ចី', icon: Icons.person,image: 'assets/ABA/Pay_Bills.png',
    tap: (){
      showDialog(context: context,
          builder: _buildComingSoon
      );
    }
    ));
    list.add(new HomeIconModel(title: 'ទីតាំង ATM', icon: Icons.person, image: 'assets/ABA/24.png',
    tap: (){
      showDialog(context: context,
          builder: _buildComingSoon
      );
    }
    ));

    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - 134,
      child: Column(
        children: [
          Expanded(
            flex: 4,
            child: Container(
              color: Colors.blueGrey,
              child: GridView.count(
                crossAxisCount: 3,
                childAspectRatio:0.9,
                shrinkWrap: true,
                ///space លើ ក្រោម
                crossAxisSpacing: 2,
                mainAxisSpacing: 2,
                physics: NeverScrollableScrollPhysics(),///scroll មិនបាន
                children: List.generate( homeIconModelBuild().length, (index){
                  return InkWell(
                    onTap: (){
                      homeIconModelBuild()[index].tap();
                    },
                    child: Container(
                      color: HexColor("#004469"),
                      child: Center(
                        child: Container(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              homeIconModelBuild()[index].image == null ? Icon(homeIconModelBuild()[index].icon, color: Colors.white,size: 50,)
                              : Image.asset(homeIconModelBuild()[index].image, width: 50, color: Colors.white,),
                              SizedBox(height: 10,),
                              Text(homeIconModelBuild()[index].title, style: TextStyle(color: Colors.white),),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }),
              ),
            ),
          ),
          Expanded(
              flex: 1,
              child: Container(
                  child: Stack(
                    children: [
                      Positioned(
                        bottom: -20,
                        right: -30,
                        child: Container(
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            //border: Border.all(width: 1, color: Colors.white),
                            //color: Colors.lightBlue[200]
                          ),
                          child: Center(child: Image.asset('assets/icon/moneytransfer.png', color: HexColor('#73d9e7'),)),
                        ),
                      ),
                      Positioned(
                        bottom: 40,
                        right: 150,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100000),
                            //border: Border.all(width: 1, color: Colors.white),
                            //color: Colors.lightBlue[200]
                          ),
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('ផ្ទេរប្រាក់រហ័ស', style: TextStyle(fontSize: 15, color: Colors.white),),
                                SizedBox(height: 5,),
                                Text('ចូលប្រើគំរូរបស់អ្នកត្រង់នេះ ដើម្បីផ្ទេរប្រាក់បានលឿន', style: TextStyle(fontSize: 10, color: Colors.white),),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  color: HexColor('#00bcd5'),
              )
          ),
          Expanded(
              flex: 1,
              child: Container(
                  child: Stack(
                    children: [
                      Positioned(
                        bottom: -20,
                        right: -30,
                        child: Container(
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100000),
                              //border: Border.all(width: 1, color: Colors.white),
                              //color: Colors.lightBlue[200]
                          ),
                          child: Center(child: Image.asset('assets/icon/moneytransfer.png', color: HexColor('#f7a1a0'),)),
                        ),
                      ),
                      Positioned(
                        bottom: 40,
                        right: 150,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100000),
                              //border: Border.all(width: 1, color: Colors.white),
                              //color: Colors.lightBlue[200]
                          ),
                          child: Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('ទូរទាត់រហ័ស', style: TextStyle(fontSize: 15, color: Colors.white),),
                                  SizedBox(height: 5,),
                                  Text('បង្កើតគំរូដើម្បីងាយស្រួលនិង រហ័សក្នុងការទូទាត់', style: TextStyle(fontSize: 10, color: Colors.white),),
                                ],
                              ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  color: HexColor('#ee5351'),
              )
          ),
        ],
      ),
    );
  }



  get _buildComingSoon{
    return Container(
      child: AlertDialog(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text("Coming Soon", textAlign: TextAlign.center, style: TextStyle(color: Colors.redAccent),),
      ),
    );
  }

}



