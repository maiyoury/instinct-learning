import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app2222/item.dart';

class ListViewBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody,
    );
  }

  get _buildBody{
    return Container(
      alignment: Alignment.center,
      color: Colors.black87,
      child: ListView(
        children: [
          _buildListView,
          _buildListView,
          _buildListView,
          _buildListView,
          _buildListView,
        ],
      ),
    );
  }

  get _buildListView{
    return Container(
      color: Colors.grey[300],
      height: 250,
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: itemList.length,
        itemBuilder: (context, index){
          return _buildListItem(itemList[index]);
        },
      ),
    );
  }

  _buildListItem(Item item){
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 170,
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            image: DecorationImage(
              image: NetworkImage(item.img),
              fit: BoxFit.cover,
            ),
          ),
        ),
        InkWell(
          onTap: (){
            print('clicked ${item.title}');
          },
          child: Container(
            height: 80,
            width: 80,
            alignment: Alignment.center,
            child: Icon(Icons.play_circle_fill, color: Colors.white60, size: 70,),
          ),
        ),
        Positioned(
          top: 20,
          child: Container(
            height: 30,
            width: 150,
            color: Colors.white60,
            alignment: Alignment.center,
            child: Text(item.title, overflow: TextOverflow.ellipsis,),
          )
        )
      ],
    );
  }



}
