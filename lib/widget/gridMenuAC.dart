import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';


class HomeIconModel{
  String title;
  IconData icon;
  String image;
  GestureTapCallback tap;
  HomeIconModel({this.icon, this.title, this.image, this.tap});
}

///model icon
List<HomeIconModel> homeIconModelBuild(){
  List<HomeIconModel> list = [];
  list.add(new HomeIconModel(title: 'គណនី', image: 'assets/icon/approve.png',));
  list.add(new HomeIconModel(title: 'គណនី', image: 'assets/icon/approveleave.png',));
  list.add(new HomeIconModel(title: 'គណនី', image: 'assets/icon/approveOTfee.png',));
  list.add(new HomeIconModel(title: 'គណនី', image: 'assets/icon/attendance.png',));
  list.add(new HomeIconModel(title: 'គណនី', image: 'assets/icon/calender.png',));
  list.add(new HomeIconModel(title: 'គណនី', image: 'assets/icon/estimate.png',));
  list.add(new HomeIconModel(title: 'គណនី', image: 'assets/icon/event.png',));
  list.add(new HomeIconModel(title: 'គណនី', image: 'assets/icon/finance.png',));
  list.add(new HomeIconModel(title: 'គណនី', image: 'assets/icon/money.png',));
  return list;
}
/// end



class GridMenuAC extends StatefulWidget {
  @override
  _GridMenuACState createState() => _GridMenuACState();
}

class _GridMenuACState extends State<GridMenuAC> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey,
      height: MediaQuery.of(context).size.height - 135,
      child: Column(
        children: [
          Container(
            height: 40,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end:
                Alignment(0.8, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Color(0xff6fb3fa),
                  Color(0xff82c8fb),
                  Color(0xff9adbfb),
                ], // red to yellow
                //tileMode: TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
            //color: HexColor('#86c9fd'),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    child: Text("អត្រាប្តូរប្រាក់",),
                  ),
                  Container(
                    child: Text("ដុល្លា",),
                  ),
                  Container(
                    child: Text("4,059.00",),
                  ),
                  Container(
                    child: Text("4,071.00",),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 6,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                //color: Colors.grey,
                child: GridView.count(
                  crossAxisCount: 3,
                  childAspectRatio: 0.9,
                  shrinkWrap: true,
                  ///space លើ ក្រោម
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 8,
                  physics: NeverScrollableScrollPhysics(),///scroll មិនបាន
                  children: List.generate( homeIconModelBuild().length, (index){
                    return InkWell(
                      onTap: (){
                        homeIconModelBuild()[index].tap();
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        ///color: Colors.white,
                        child: Center(
                          child: Container(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                homeIconModelBuild()[index].image == null ? Icon(homeIconModelBuild()[index].icon, color: Colors.redAccent ,size: 50,)
                                    : Image.asset(homeIconModelBuild()[index].image, width: 50,),
                                SizedBox(height: 10,),
                                Text(homeIconModelBuild()[index].title, style: TextStyle(color: Colors.black87),),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              color: Colors.green,
            ),
          ),
        ],
      ),
    );
  }
}
