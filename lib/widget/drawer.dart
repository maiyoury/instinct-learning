import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_app2222/g.dart' as g;

class CoDrawer extends StatelessWidget {
  String path = 'assets/icon/';
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Drawer(
        child: Container(
          color: HexColor("#004469"),
          child: ListView(
            children: [
              InkWell(
                onTap: (){
                  ///action button
                },
                child: Container(
                  height: 90,
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  color: HexColor("#004469"),
                  child: Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            border: Border.all(color: Colors.white, width: 2)
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100.0),
                          child: Image.asset(
                            '${path}din.jpg',
                            height: 60,
                            width: 60,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(width: 20,),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 5,),
                          Text('សូមស្វាគមន៍', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                          SizedBox(height: 7,),
                          Text('YOURY MAI', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                          SizedBox(height: 5,),
                          Text('Mobile ID : 556677', style: TextStyle(color: Colors.white, fontSize: 9),)
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Divider(color: Colors.white,),
              ListTile(
                leading: Icon(Icons.location_on_rounded, color: Colors.white,),
                title: Text('កន្លែងទូទាត់តាម ABA Pay', style: TextStyle(color: Colors.white),),
              ),
              ListTile(
                leading: Icon(Icons.monetization_on_outlined, color: Colors.white,),
                title: Text('អត្រាប្តូរប្រាក់', style: TextStyle(color: Colors.white)),
              ),
              ListTile(
                leading: Icon(Icons.phone_in_talk, color: Colors.white,),
                title: Text('ទំនាក់ទំនងមកយើងខ្ញុំ', style: TextStyle(color: Colors.white)),
              ),
              ListTile(
                leading: Icon(Icons.settings, color: Colors.white,),
                title: Text('ការកំណត់ផ្សេងៗ', style: TextStyle(color: Colors.white)),
              ),
              Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height/2.8),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Column(
                      children: [
                        Divider(color: Colors.white,),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                child: Text("V 8.5.19", style: TextStyle(color: Colors.white),),
                              ),
                              Container(
                                child: Text("Login ចុងក្រោយ : 12:49 | 30 Dec 21", style: TextStyle(color: Colors.blueGrey),),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
